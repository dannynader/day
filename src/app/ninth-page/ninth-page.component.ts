import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-ninth-page',
  templateUrl: './ninth-page.component.html',
  styleUrls: ['./ninth-page.component.scss']
})
export class NinthPageComponent implements OnInit {

  ninthpage:FormGroup

  constructor(private formbuilder:FormBuilder, private webData: WebdataService) {
    this.ninthpage = this.formbuilder.group({
      name:[''],
      Name_Father:[''],
      last_name:[''],
      mother_last_name:[''],
      Birth:[''],
      record:['']
    });

   }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.ninthpage.controls['name'].setValue(data.name)
      this.ninthpage.controls['Name_Father'].setValue(data.Name_Father)
      this.ninthpage.controls['last_name'].setValue(data.last_name)
      this.ninthpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.ninthpage.controls['Birth'].setValue(data.Birth)
      this.ninthpage.controls['record'].setValue(data.Birth)
    })
  }

}
