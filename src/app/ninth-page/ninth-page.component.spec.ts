import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NinthPageComponent } from './ninth-page.component';

describe('NinthPageComponent', () => {
  let component: NinthPageComponent;
  let fixture: ComponentFixture<NinthPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NinthPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NinthPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
