import {Component, EventEmitter} from '@angular/core';
import {WebdataService} from './webdata.service';
import {SearchComponent} from './component/search/search.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'adminaineldelb';
  isShowing = false;
  names;
  isSearch = true;

  constructor(private webdata: WebdataService, public dialog: MatDialog) {
  }

  onShow() {
    // this.isShowing = !this.isShowing;
    // this.webdata.isShowing.next(this.isShowing);
    this.openDialog();
    this.isSearch = false;
  }

  getName(value: any) {
    if (value.name === '') {
      this.names = '';
    } else {
      this.webdata.getName(value).subscribe((data) => {
        this.names = data;
      });
    }
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(SearchComponent, {
      width: '700px',
      autoFocus: false,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.isSearch = true;
    });
  }

  onPrint() {
    window.print();
  }

}
