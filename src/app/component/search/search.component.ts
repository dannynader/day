import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {WebdataService} from '../../webdata.service';
import {MatDialog} from '@angular/material';
import {Name} from '../../module/name';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  // isShowing = false;
  searchName: FormGroup;
  @Output() SearchName = new EventEmitter();
  names;
  isloaed = false;

  serachtypes = [
    {name: 'الاسم', value: 'name'},
    {name: 'اسم الأب', value: 'father_name'},
    {name: 'الشهرة', value: 'last_name'},
    {name: 'تاريخ الولادة', value: 'dob'},
    {name: 'رقم الهاتف', value: 'phone_number'},
  ];


  constructor(private fb: FormBuilder, private webData: WebdataService, public dialog: MatDialog) {
    this.searchName = this.fb.group({
      name: [''],
      serachType: ['']
    });
  }

  ngOnInit() {
    // this.webData.isShowing.subscribe((data) => {
    //   this.isShowing = data;
    // });
  }

  onShowing() {
    this.isloaed = true;
    this.webData.getName(this.searchName.value).subscribe((names) => {
      console.log(names);
      this.names = names;
      this.isloaed = false;
    });
    // this.SearchName.emit(this.searchName.value);
  }

  onSelectedName(value) {
    this.webData.getNameValue.next(value);
    // this.webData.isShowing.next(false);
    this.searchName.controls['name'].setValue('');
    this.dialog.closeAll();
    // this.onShowing();
  }

}
