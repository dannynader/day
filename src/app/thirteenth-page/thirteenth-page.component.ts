import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-thirteenth-page',
  templateUrl: './thirteenth-page.component.html',
  styleUrls: ['./thirteenth-page.component.scss']
})
export class ThirteenthPageComponent implements OnInit {

  thirteenpage:FormGroup

  constructor(private formbuilder:FormBuilder, private webData: WebdataService) { 
  
    this.thirteenpage = this.formbuilder.group({
      name:[''],
      Name_Father:[''],
      last_name:[''],
      mother_last_name:[''],
      Nationality:[''],
      Birth:[''],
      record:['']
  });
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.thirteenpage.controls['name'].setValue(data.name)
      this.thirteenpage.controls['Name_Father'].setValue(data.Name_Father)
      this.thirteenpage.controls['last_name'].setValue(data.last_name)
      this.thirteenpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.thirteenpage.controls['Nationality'].setValue(data.Nationality)
      this.thirteenpage.controls['Birth'].setValue(data.Birth)
      this.thirteenpage.controls['record'].setValue(data.record)
    })
  }

}
