import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirteenthPageComponent } from './thirteenth-page.component';

describe('ThirteenthPageComponent', () => {
  let component: ThirteenthPageComponent;
  let fixture: ComponentFixture<ThirteenthPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirteenthPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirteenthPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
