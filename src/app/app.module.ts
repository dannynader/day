import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FirstPageComponent} from './first-page/first-page.component';
import {HttpClientModule} from '@angular/common/http';
import {SecondPageComponent} from './second-page/second-page.component';
import {ThirdPageComponent} from './third-page/third-page.component';
import {FourthPageComponent} from './fourth-page/fourth-page.component';
import {FifthPageComponent} from './fifth-page/fifth-page.component';
import {SixthPageComponent} from './sixth-page/sixth-page.component';
import {SeventhPageComponent} from './seventh-page/seventh-page.component';
import {EighthPageComponent} from './eighth-page/eighth-page.component';
import {NinthPageComponent} from './ninth-page/ninth-page.component';
import {TenthPageComponent} from './tenth-page/tenth-page.component';
import {EleventhPageComponent} from './eleventh-page/eleventh-page.component';
import {TwelvethPageComponent} from './twelveth-page/twelveth-page.component';
import {ThirteenthPageComponent} from './thirteenth-page/thirteenth-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from './home/home.component';
import {SearchComponent} from './component/search/search.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule, MatProgressSpinnerModule} from '@angular/material';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstPageComponent,
    SecondPageComponent,
    ThirdPageComponent,
    FourthPageComponent,
    FifthPageComponent,
    SixthPageComponent,
    SeventhPageComponent,
    EighthPageComponent,
    NinthPageComponent,
    TenthPageComponent,
    EleventhPageComponent,
    TwelvethPageComponent,
    ThirteenthPageComponent,
    HomeComponent,
    SearchComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    BrowserAnimationsModule,
    MatDialogModule,
    MatProgressSpinnerModule

  ],
  entryComponents: [
    SearchComponent,
    AlertComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
