import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwelvethPageComponent } from './twelveth-page.component';

describe('TwelvethPageComponent', () => {
  let component: TwelvethPageComponent;
  let fixture: ComponentFixture<TwelvethPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwelvethPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwelvethPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
