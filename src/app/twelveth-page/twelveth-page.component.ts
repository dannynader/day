import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-twelveth-page',
  templateUrl: './twelveth-page.component.html',
  styleUrls: ['./twelveth-page.component.scss']
})
export class TwelvethPageComponent implements OnInit {

twelvethpage:FormGroup

  constructor(private formbuilder: FormBuilder, private webData: WebdataService) { 
    this.twelvethpage = this.formbuilder.group({
      name:[''],
      Name_Father:[''],
      last_name:[''],
      mother_last_name:[''],
      Nationality:[''],
      Birth:['']
    });

  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.twelvethpage.controls['name'].setValue(data.name)
      this.twelvethpage.controls['Name_Father'].setValue(data.Name_Father)
      this.twelvethpage.controls['last_name'].setValue(data.last_name)
      this.twelvethpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.twelvethpage.controls['Nationality'].setValue(data.Nationality)
      this.twelvethpage.controls['Birth'].setValue(data.Birth)
    })
  }

}
