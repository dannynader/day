import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-sixth-page',
  templateUrl: './sixth-page.component.html',
  styleUrls: ['./sixth-page.component.scss']
})
export class SixthPageComponent implements OnInit {
sixthpage:FormGroup

  constructor(private formbuilder:FormBuilder,private webData: WebdataService) { 
    this.sixthpage = this.formbuilder.group({
      Full_name:[''],
      mother_last_name:[''],
      phone: [''],
      record:['']
    });
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.sixthpage.controls['Full_name'].setValue(data.name?(data.name +' '+ data.Name_Father +' '+ data.last_name):'');
      this.sixthpage.controls['mother_last_name'].setValue(data.mother_last_name);
      this.sixthpage.controls['phone'].setValue(data.phone);
      this.sixthpage.controls['record'].setValue(data.record);
    })
  }

}
