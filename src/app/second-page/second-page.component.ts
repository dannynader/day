import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {WebdataService} from '../webdata.service';
import {Name} from "../module/name";

@Component({
  selector: 'app-second-page',
  templateUrl: './second-page.component.html',
  styleUrls: ['./second-page.component.scss']
})
export class SecondPageComponent implements OnInit {

  secondPage: FormGroup;
  names;

  constructor(private formbuilder: FormBuilder, private  webData: WebdataService) {
    this.secondPage = this.formbuilder.group({
      Name_Father:[''],
      name: [''],
      mother_last_name: [''],
      last_name: [''],
      Birth: [''],
      record:['']
    });
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.secondPage.controls['Name_Father'].setValue(data.Name_Father)
      this.secondPage.controls['name'].setValue(data.name)
      this.secondPage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.secondPage.controls['last_name'].setValue(data.last_name)
      this.secondPage.controls['Birth'].setValue(data.Birth)
      this.secondPage.controls['record'].setValue(data.record)
    })
  }

}
