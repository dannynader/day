import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-third-page',
  templateUrl: './third-page.component.html',
  styleUrls: ['./third-page.component.scss']
})
export class ThirdPageComponent implements OnInit {
thirdpage:FormGroup

  constructor(
    private formbuilder:FormBuilder,
    private webData: WebdataService
  ) {

    this.thirdpage = this.formbuilder.group({
      last_name:[''],
      Name_Father:[''],
      name:[''],
      Birth:[''],
      mother_last_name:[''],
      Doctrine:[''],
      phone:[''],
      record:['']
    })

   }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.thirdpage.controls['last_name'].setValue(data.last_name)
      this.thirdpage.controls['Name_Father'].setValue(data.Name_Father)
      this.thirdpage.controls['name'].setValue(data.name)
      this.thirdpage.controls['Birth'].setValue(data.Birth)
      this.thirdpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.thirdpage.controls['Doctrine'].setValue(data.Doctrine)
      this.thirdpage.controls['phone'].setValue(data.phone)
      this.thirdpage.controls['record'].setValue(data.record)
    })
    
  }

}
