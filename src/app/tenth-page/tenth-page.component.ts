import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-tenth-page',
  templateUrl: './tenth-page.component.html',
  styleUrls: ['./tenth-page.component.scss']
})
export class TenthPageComponent implements OnInit {

tenthpage:FormGroup 

constructor(private formbuilder:FormBuilder, private webData: WebdataService) { 
  this.tenthpage = this.formbuilder.group({
    last_name:[''],
    name:[''],
    mother_last_name:[''],
    Name_Father:[''],
    Birth:[''],
    phone:[''],
    record:['']
  });
}

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.tenthpage.controls['last_name'].setValue(data.last_name)
      this.tenthpage.controls['name'].setValue(data.name)
      this.tenthpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.tenthpage.controls['Name_Father'].setValue(data.Name_Father)
      this.tenthpage.controls['Birth'].setValue(data.Birth)
      this.tenthpage.controls['phone'].setValue(data.phone)
      this.tenthpage.controls['record'].setValue(data.record)
    })
  }

}
