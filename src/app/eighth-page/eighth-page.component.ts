import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-eighth-page',
  templateUrl: './eighth-page.component.html',
  styleUrls: ['./eighth-page.component.scss']
})
export class EighthPageComponent implements OnInit {

  eighthpage:FormGroup

  constructor(private formbuilder: FormBuilder, private webData: WebdataService) { 

    this.eighthpage = this.formbuilder.group({
      Name_Father:[''],
      name:[''],
      mother_last_name:[''],
      last_name:[''],
      Sex:[''],
      Birth:[''],
      record:['']
    });

  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.eighthpage.controls['Name_Father'].setValue(data.Name_Father)
      this.eighthpage.controls['name'].setValue(data.name)
      this.eighthpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.eighthpage.controls['last_name'].setValue(data.last_name)
      this.eighthpage.controls['Sex'].setValue(data.Sex)
      this.eighthpage.controls['Birth'].setValue(data.Birth)
      this.eighthpage.controls['record'].setValue(data.record)
    })
  }

}
