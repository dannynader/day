import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {WebdataService} from '../webdata.service';
import {Name} from '../module/name';
import {MatDialog} from '@angular/material';
import {AlertComponent} from '../alert/alert.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isPreosn = false;
  homepage: FormGroup;
  name: Name;

  constructor(private formbuilder: FormBuilder, private webData: WebdataService, public dialog: MatDialog) {

    this.homepage = this.formbuilder.group({
      name: [''],
      Name_Father: [''],
      last_name: [''],
      mother_last_name: [''],
      Birth: [''],
      record:[''],
      Sex: [''],
      Doctrine: [''],
      Nationality: [''],
      id: [''],
      phone: ['']
    });
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data: Name) => {
      this.homepage.controls['name'].setValue(data.name);
      this.homepage.controls['last_name'].setValue(data.last_name);
      this.homepage.controls['mother_last_name'].setValue(data.mother_last_name);
      this.homepage.controls['Birth'].setValue(data.Birth);
      this.homepage.controls['record'].setValue(data.record);
      this.homepage.controls['Sex'].setValue(data.Sex);
      this.homepage.controls['Doctrine'].setValue(data.Doctrine);
      this.homepage.controls['Nationality'].setValue(data.Nationality);
      this.homepage.controls['phone'].setValue(data.phone);
      this.homepage.controls['id'].setValue(data.id);
      this.homepage.controls['Name_Father'].setValue(data.Name_Father);
    });
  }

  onCreateNew() {
    this.isPreosn = true;
    this.webData.createNewUser(this.homepage.value).subscribe((data) => {
      this.isPreosn = false;
      this.openDialog('تم إضافة شخص جديد');
    });
  }

  onEditUser() {
    this.isPreosn = true;
    this.webData.editUser(this.homepage.value).subscribe((data) => {
      this.isPreosn = false;
      this.openDialog('تم تعديل هذا الشخص');
    });
  }


  openDialog(data): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '700px',
      autoFocus: false,
      maxHeight: '90vh',
      data: {mess: data}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
