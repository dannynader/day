export interface Name {
  id:number,
  Nationality: string,
  ID_Number: string,
  phone: string,
  name: string,
  Name_Father: string,
  mother_last_name: string,
  last_name: string
  Sex: string
  Birth: string
  record: string,
  Doctrine: string
}
