import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-seventh-page',
  templateUrl: './seventh-page.component.html',
  styleUrls: ['./seventh-page.component.scss']
})
export class SeventhPageComponent implements OnInit {

  seventhpage:FormGroup

  constructor(private formbuilder: FormBuilder, private webData: WebdataService) {

    this.seventhpage = this.formbuilder.group({
      Full_name:[''],
      Birth:[''],
      phone:['']
    });

   }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.seventhpage.controls['Full_name'].setValue(data.name?(data.name.trim() +' '+ data.Name_Father.trim() +' '+ data.last_name.trim()):'');
      this.seventhpage.controls['Birth'].setValue(data.Birth);
      this.seventhpage.controls['phone'].setValue(data.phone);
    })
  }

}
