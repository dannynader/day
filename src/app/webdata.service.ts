import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class WebdataService {

  isShowing = new BehaviorSubject(false);
  getNameValue = new BehaviorSubject({});
  isSearch = new BehaviorSubject(true);

  api = 'http://mokhtar.aineldelb.gov.lb/admin/api/';

  constructor(private http: HttpClient) {
  }

  getName(value) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(this.api + 'findbyname', value, httpOptions);
  }

  createNewUser(value) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(this.api + 'createnewuser', value, httpOptions);
  }

  editUser(value) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.post(this.api + 'update', value, httpOptions);
  }


}
