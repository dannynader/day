import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EleventhPageComponent } from './eleventh-page.component';

describe('EleventhPageComponent', () => {
  let component: EleventhPageComponent;
  let fixture: ComponentFixture<EleventhPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EleventhPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EleventhPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
