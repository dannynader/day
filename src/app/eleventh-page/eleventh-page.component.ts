import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-eleventh-page',
  templateUrl: './eleventh-page.component.html',
  styleUrls: ['./eleventh-page.component.scss']
})
export class EleventhPageComponent implements OnInit {

  elventhpage:FormGroup

  constructor(private formbuilder:FormBuilder, private webData: WebdataService) { 

    this.elventhpage = this.formbuilder.group({
      name:[''],
      Name_Father:[''],
      last_name:[''],
      mother_last_name:[''],
      Nationality:[''],
      Birth:[''],
      record:['']
    });
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.elventhpage.controls['name'].setValue(data.name)
      this.elventhpage.controls['Name_Father'].setValue(data.Name_Father)
      this.elventhpage.controls['last_name'].setValue(data.last_name)
      this.elventhpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.elventhpage.controls['Nationality'].setValue(data.Nationality)
      this.elventhpage.controls['Birth'].setValue(data.Birth)
      this.elventhpage.controls['record'].setValue(data.record)
    })
  }

}
