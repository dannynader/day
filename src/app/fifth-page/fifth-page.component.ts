import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-fifth-page',
  templateUrl: './fifth-page.component.html',
  styleUrls: ['./fifth-page.component.scss']
})
export class FifthPageComponent implements OnInit {

  fifthpage:FormGroup

  constructor(
    private formbuilder:FormBuilder,
    private webData: WebdataService
  ) {
    this.fifthpage =  this.formbuilder.group({
      name:[''],
      last_name :[''],
      Name_Father:[''],
      Birth:[''],
      mother_last_name:[''],
      Sex:[''],
      
    });
    
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.fifthpage.controls['name'].setValue(data.name)
      this.fifthpage.controls['last_name'].setValue(data.last_name)
      this.fifthpage.controls['Name_Father'].setValue(data.Name_Father)
      this.fifthpage.controls['Birth'].setValue(data.Birth)
      this.fifthpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.fifthpage.controls['Sex'].setValue(data.Sex)
    })
  }

}
