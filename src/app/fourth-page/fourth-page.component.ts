import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {WebdataService} from "../webdata.service";
import {Name} from "../module/name";

@Component({
  selector: 'app-fourth-page',
  templateUrl: './fourth-page.component.html',
  styleUrls: ['./fourth-page.component.scss']
})
export class FourthPageComponent implements OnInit {

  fourthpage:FormGroup

  constructor(
    private formbuilder:FormBuilder,
    private webData: WebdataService
  ) { 
    this.fourthpage = this.formbuilder.group({
      name:[''],
      Name_Father:[''],
      last_name:[''],
      mother_last_name:[''],
      Birth:[''],
      Sex:[''],
      record:['']
    });
  }

  ngOnInit() {
    this.webData.getNameValue.subscribe((data:Name)=>{
      this.fourthpage.controls['name'].setValue(data.name)
      this.fourthpage.controls['Name_Father'].setValue(data.Name_Father)
      this.fourthpage.controls['last_name'].setValue(data.last_name)
      this.fourthpage.controls['mother_last_name'].setValue(data.mother_last_name)
      this.fourthpage.controls['Birth'].setValue(data.Birth)
      this.fourthpage.controls['Sex'].setValue(data.Sex)
      this.fourthpage.controls['record'].setValue(data.record)
    })
  }

}
