import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FirstPageComponent} from './first-page/first-page.component';
import {SecondPageComponent} from './second-page/second-page.component';
import {ThirdPageComponent} from './third-page/third-page.component';
import {FourthPageComponent} from './fourth-page/fourth-page.component';
import {FifthPageComponent} from './fifth-page/fifth-page.component';
import {SixthPageComponent} from './sixth-page/sixth-page.component';
import {SeventhPageComponent} from './seventh-page/seventh-page.component';
import {EighthPageComponent} from './eighth-page/eighth-page.component';
import {NinthPageComponent} from './ninth-page/ninth-page.component';
import {TenthPageComponent} from './tenth-page/tenth-page.component';
import {EleventhPageComponent} from './eleventh-page/eleventh-page.component';
import {TwelvethPageComponent} from './twelveth-page/twelveth-page.component';
import {ThirteenthPageComponent} from './thirteenth-page/thirteenth-page.component';
import {HomeComponent} from './home/home.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'page1',
    component: FirstPageComponent
  },
  {
    path: 'page2',
    component: SecondPageComponent
  },
  {
    path: 'page3',
    component: ThirdPageComponent
  },
  {
    path: 'page4',
    component: FourthPageComponent
  },
  {
    path: 'page5',
    component: FifthPageComponent
  },
  {
    path: 'page6',
    component: SixthPageComponent
  },
  {
    path: 'page7',
    component: SeventhPageComponent
  },
  {
    path: 'page8',
    component: EighthPageComponent
  },
  {
    path: 'page9',
    component: NinthPageComponent
  },
  {
    path: 'page10',
    component: TenthPageComponent
  },
  {
    path: 'page11',
    component: EleventhPageComponent
  },
  {
    path: 'page12',
    component: TwelvethPageComponent
  },
  {
    path: 'page13',
    component: ThirteenthPageComponent
  }, {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
